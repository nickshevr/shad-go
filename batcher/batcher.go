// +build !solution

package batcher

import (
	"gitlab.com/slon/shad-go/batcher/slow"
	"sync"
)

type Batcher struct {
	mu sync.Mutex
	cond *sync.Cond
	val *slow.Value
	running bool
	lastValue interface{}
}

func NewBatcher(v *slow.Value) *Batcher {
	b := &Batcher{
		val: v,
	}

	b.cond = sync.NewCond(&b.mu)

	return b
}

func (b *Batcher) Load() interface{} {
	b.mu.Lock()
	defer b.mu.Unlock()

	if b.running {
		b.cond.Wait()

		return b.lastValue
	}

	b.running = true
	b.mu.Unlock()
	b.lastValue = b.val.Load()
	b.mu.Lock()
	b.running = false
	b.lastValue = b.val.Load()
	b.cond.Broadcast()

	return b.lastValue
}