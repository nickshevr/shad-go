// +build !solution

package main

import (
	"bytes"
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"net/http"
	"strconv"
	"time"
)

func getConstant(symbol rune) string {
	if symbol == '0' {
		return Zero
	}

	if symbol == '1' {
		return One
	}

	if symbol == '2' {
		return Two
	}

	if symbol == '3' {
		return Three
	}

	if symbol == '4' {
		return Four
	}

	if symbol == '5' {
		return Five
	}

	if symbol == '6' {
		return Six
	}

	if symbol == '7' {
		return Seven
	}

	if symbol == '8' {
		return Eight
	}

	if symbol == '9' {
		return Nine
	}

	return Colon
}

func printConstant(symbol rune, k int, img *image.RGBA, offset int) {
	x := offset
	y := 0

	constant := getConstant(symbol)

	for _, dot := range constant {
		// .
		if dot == 46 {
			for i := 0; i <= k; i++ {
				for j := 0; j <= k; j++ {
					img.Set(x+i, y+j, color.White)
				}
			}
		}

		// 1
		if dot == 49 {
			for i := 0; i <= k; i++ {
				for j := 0; j <= k; j++ {
					img.Set(x+i, y+j, Cyan)
				}
			}
		}

		x += k
		// \n
		if dot == 10 {
			y += k
			x = offset
		}
	}
}

func createPng(time string, k int) *image.RGBA {
	h := 12
	wColon := 4
	w := 8
	offset := 0

	height := h * k
	width := (6*w + 2*wColon) * k

	img := image.NewRGBA(image.Rect(0, 0, width, height))

	for _, symbol := range time {
		printConstant(symbol, k, img, offset)

		if symbol == ':' {
			offset += wColon * k
		} else {
			offset += w * k
		}
	}

	return img
}

func main() {
	portPtr := flag.String("port", "6029", "setting a port for web server")
	flag.Parse()

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		var k int
		var t string

		query := req.URL.Query()

		_, hasTime := query["time"]

		if !hasTime {
			t = time.Now().Format("15:04:05")
		} else {
			queryTime := query.Get("time")
			tmp, e := time.Parse("15:04:05", queryTime)

			if e != nil || len(queryTime) != 8 {
				w.WriteHeader(400)
				fmt.Fprintf(w, "invalid time")

				return
			}

			t = tmp.Format("15:04:05")
		}

		k, err := strconv.Atoi(query.Get("k"))

		if err != nil || k > 30 || k < 1 {
			w.WriteHeader(400)
			fmt.Fprintf(w, "invalid k")

			return
		}

		dateImage := createPng(t, k)

		buf := new(bytes.Buffer)
		err = png.Encode(buf, dateImage)

		if err != nil {
			w.WriteHeader(500)

			return
		}

		w.Header().Add("Content-Type", "image/png")
		w.WriteHeader(200)
		w.Header().Set("Content-Length", strconv.Itoa(len(buf.Bytes())))
		if _, err := w.Write(buf.Bytes()); err != nil {
			log.Println("unable to write dateImage.")
		}
	})

	log.Fatal(http.ListenAndServe(":"+*portPtr, mux))
}
