// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func fetchURL(url string, messages chan string) {
	resp, err := http.Get(url)

	if err != nil {
		messages <- err.Error()

		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		messages <- err.Error()

		return
	}

	messages <- string(body)
}

func main() {
	urls := os.Args[1:]
	messages := make(chan string, len(urls))

	for _, url := range urls {
		go fetchURL(url, messages)
	}

	for _, url := range urls {
		msg := <-messages
		fmt.Printf("%s %s", url, msg)
	}
}
