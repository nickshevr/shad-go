// +build !solution

package hogwarts

const WHITE = 0
const GREY = 1
const BLACK = 2

type Graph = map[string][]string
type GraphState = map[string]int

var vertexState GraphState

var result = []string{}

func processVertex(vertex string, graph Graph, graphState GraphState) {
	currentState := graphState[vertex]

	if currentState == WHITE {
		graphState[vertex] = GREY

		siblings := graph[vertex]

		for _, sibling := range siblings {
			_, siblingExists := graph[sibling]
			if !siblingExists {
				graph[sibling] = []string{}
			}

			processVertex(sibling, graph, graphState)
		}

		result = append(result, vertex)
		graphState[vertex] = BLACK

		return
	}

	if currentState == GREY {
		panic("Cycle!")
	}

	if currentState == BLACK {
		return
	}
}

func GetCourseList(prereqs Graph) []string {
	vertexState = make(GraphState)

	for k := range prereqs {
		processVertex(k, prereqs, vertexState)
	}

	return result
}
