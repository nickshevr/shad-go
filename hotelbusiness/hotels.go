// +build !solution

package hotelbusiness

import "sort"

type Guest struct {
	CheckInDate  int
	CheckOutDate int
}

type Load struct {
	StartDate  int
	GuestCount int
}

var dateMap map[int]int

func ComputeLoad(guests []Guest) []Load {
	result := []Load{}
	dateMap = make(map[int]int)

	for _, guest := range guests {
		start := guest.CheckInDate
		end := guest.CheckOutDate

		_, found := dateMap[end]

		if !found {
			dateMap[end] = 0
		}

		for i := start; i < end; i++ {
			dateMap[i]++
		}
	}

	for date, guestsCount := range dateMap {
		previousCount := dateMap[date-1]

		if previousCount != guestsCount {
			result = append(result, Load{
				StartDate:  date,
				GuestCount: guestsCount,
			})
		}
	}

	sort.SliceStable(result, func(i, j int) bool {
		return result[i].StartDate < result[j].StartDate
	})

	return result
}
