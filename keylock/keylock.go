// +build !solution

package keylock

import (
	"sort"
	"sync"
)

type KeyLock struct {
	Map map[string]chan struct{}
	mux sync.Mutex
}

func New() *KeyLock {
	return &KeyLock{
		Map: make(map[string]chan struct{}, 1024),
	}
}

func (l *KeyLock) UpdateKeys(keys []string) {
	l.mux.Lock()

	for _, key := range keys {
		l.Map[key] = make(chan struct{})
	}

	l.mux.Unlock()
}

func (l *KeyLock) FreeKeys(keys []string) {
	l.mux.Lock()
	for _, key := range keys {
		if _, has := l.Map[key]; has {
			close(l.Map[key])
			delete(l.Map, key)
		}
	}
	l.mux.Unlock()
}

func (l *KeyLock) FreeKey(key string) {
	l.mux.Lock()
	close(l.Map[key])
	l.mux.Unlock()
}

func (l *KeyLock) LockKeys(keys []string, cancel <-chan struct{}) (canceled bool, unlock func()) {
	var locked []string

	sort.SliceStable(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})

	for _, key := range keys {
		l.mux.Lock()

		if l.Map[key] == nil {
			l.Map[key] = make(chan struct{}, 1)
			l.Map[key] <- struct{}{}
		}

		keyLock := l.Map[key]
		l.mux.Unlock()

		select {
		case <-cancel:
			l.FreeKeys(locked)

			return true, nil
		case <-keyLock:
			locked = append(locked, key)

			continue
		}
	}

	return false, func() {
		l.FreeKeys(locked)
	}
}
