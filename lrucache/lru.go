// +build !solution

package lrucache

import "container/list"

func New(cap int) Cache {
	return &LRUCache{
		Cap:  cap,
		Size: 0,
		List: list.New(),
		Map:  make(map[int]*list.Element, cap),
	}
}

type Node struct {
	Key   int
	Value int
}

type LRUCache struct {
	Cap  int
	Size int
	List *list.List
	Map  map[int]*list.Element
}

func (c *LRUCache) Get(key int) (int, bool) {
	if elem, hit := c.Map[key]; hit {
		c.List.MoveToFront(elem)

		return elem.Value.(*Node).Value, true
	}

	return 0, false
}

func (c *LRUCache) Set(key, value int) {
	if c.Cap == 0 {
		return
	}

	if elem, ok := c.Map[key]; ok {
		c.List.MoveToFront(elem)
		elem.Value.(*Node).Value = value
		return
	}

	if c.Size <= c.Cap {
		c.List.PushFront(&Node{
			Key:   key,
			Value: value,
		})
		c.Map[key] = c.List.Front()
		c.Size++
	}

	if c.Size > c.Cap {
		elemToDelete := c.List.Back()
		delete(c.Map, elemToDelete.Value.(*Node).Key)
		c.List.Remove(elemToDelete)
		c.Size--
	}
}

func (c *LRUCache) Range(f func(key, value int) bool) {
	var currentNode = c.List.Back()

	for {
		if currentNode == nil {
			break
		}

		res := f(currentNode.Value.(*Node).Key, currentNode.Value.(*Node).Value)

		if !res {
			return
		}

		currentNode = currentNode.Prev()
	}
}

func (c *LRUCache) Clear() {
	c.List = list.New()
	c.Map = make(map[int]*list.Element)
	c.Size = 0
}
