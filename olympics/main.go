// +build !solution

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
)

type InputAthlete struct {
	Name    string `json:"athlete"`
	Age     int    `json:"age"`
	Country string `json:"country"`
	Date    string `json:"date"`
	Year    int    `json:"year"`
	Sport   string `json:"sport"`
	Gold    int    `json:"gold"`
	Silver  int    `json:"silver"`
	Bronze  int    `json:"bronze"`
	Total   int    `json:"total"`
}

type MedalsInfo struct {
	Gold   int `json:"gold"`
	Silver int `json:"silver"`
	Bronze int `json:"bronze"`
	Total  int `json:"total"`
}

type ParsedAthlete struct {
	Name         string             `json:"athlete"`
	Country      string             `json:"country"`
	Sport        string             `json:"-"`
	Medals       *MedalsInfo        `json:"medals"`
	MedalsByYear map[int]MedalsInfo `json:"medals_by_year"`
}

type Country struct {
	Country string `json:"country"`
	Gold    int    `json:"gold"`
	Silver  int    `json:"silver"`
	Bronze  int    `json:"bronze"`
	Total   int    `json:"total"`
}

var (
	mu               sync.Mutex
	fileData         []InputAthlete
	countryMap       map[int]map[string]*MedalsInfo
	countryData      map[int][]Country
	sortedBySport    map[string][]ParsedAthlete
	preparedAthletes map[string]ParsedAthlete
)

func main() {
	dataPathPtr := flag.String("data", "/Users/nickshevr/Documents/shad/shad-go/olympics/testdata/olympicWinners.json", "setting a path to data")
	portPtr := flag.String("port", "6029", "setting a port for web server")
	flag.Parse()

	file, _ := ioutil.ReadFile(*dataPathPtr)

	err := json.Unmarshal(file, &fileData)
	countryMap = make(map[int]map[string]*MedalsInfo)
	countryData = make(map[int][]Country)
	sortedBySport = make(map[string][]ParsedAthlete)
	preparedAthletes = make(map[string]ParsedAthlete)

	if err != nil {
		print(err)
	}

	for _, athlete := range fileData {
		_, found := preparedAthletes[athlete.Name]

		if !found {
			preparedAthletes[athlete.Name] = ParsedAthlete{
				Name:    athlete.Name,
				Country: athlete.Country,
				Sport:   athlete.Sport,
				Medals: &MedalsInfo{
					Gold:   athlete.Gold,
					Silver: athlete.Silver,
					Bronze: athlete.Bronze,
					Total:  athlete.Total,
				},
				MedalsByYear: make(map[int]MedalsInfo),
			}

			preparedAthletes[athlete.Name].MedalsByYear[athlete.Year] = MedalsInfo{
				Gold:   athlete.Gold,
				Silver: athlete.Silver,
				Bronze: athlete.Bronze,
				Total:  athlete.Total,
			}
		} else {
			if preparedAthletes[athlete.Name].Sport != athlete.Sport {
				continue
			}

			preparedAthletes[athlete.Name].Medals.Gold += athlete.Gold
			preparedAthletes[athlete.Name].Medals.Silver += athlete.Silver
			preparedAthletes[athlete.Name].Medals.Bronze += athlete.Bronze
			preparedAthletes[athlete.Name].Medals.Total += athlete.Total

			preparedAthletes[athlete.Name].MedalsByYear[athlete.Year] = MedalsInfo{
				Gold:   athlete.Gold,
				Silver: athlete.Silver,
				Bronze: athlete.Bronze,
				Total:  athlete.Total,
			}
		}
	}

	for _, athlete := range preparedAthletes {
		sortedBySport[athlete.Sport] = append(sortedBySport[athlete.Sport], athlete)
	}

	for _, athletes := range sortedBySport {
		sort.SliceStable(athletes, func(i, j int) bool {
			left := athletes[i]
			right := athletes[j]

			if left.Medals.Gold == right.Medals.Gold {
				if left.Medals.Silver == right.Medals.Silver {
					if left.Medals.Bronze == right.Medals.Bronze {
						return strings.Compare(left.Name, right.Name) == -1
					}

					return left.Medals.Bronze > right.Medals.Bronze
				}

				return left.Medals.Silver > right.Medals.Silver
			}

			return left.Medals.Gold > right.Medals.Gold
		})
	}

	for _, athlete := range fileData {
		country := athlete.Country
		year := athlete.Year

		_, foundCountry := countryMap[year]
		if !foundCountry {
			countryMap[year] = make(map[string]*MedalsInfo)
		}

		medalInfo, found := countryMap[year][country]

		if !found {
			countryMap[year][country] = &MedalsInfo{
				Gold:   athlete.Gold,
				Silver: athlete.Silver,
				Bronze: athlete.Bronze,
				Total:  athlete.Total,
			}
		} else {
			medalInfo.Gold += athlete.Gold
			medalInfo.Silver += athlete.Silver
			medalInfo.Bronze += athlete.Bronze
			medalInfo.Total += athlete.Total
		}
	}

	for year, countryStorage := range countryMap {
		for country, medalInfo := range countryStorage {
			countryData[year] = append(countryData[year], Country{
				Country: country,
				Gold:    medalInfo.Gold,
				Silver:  medalInfo.Silver,
				Bronze:  medalInfo.Bronze,
				Total:   medalInfo.Total,
			})
		}
	}

	for _, countries := range countryData {
		sort.SliceStable(countries, func(i, j int) bool {
			left := countries[i]
			right := countries[j]

			if left.Gold == right.Gold {
				if left.Silver == right.Silver {
					if left.Bronze == right.Bronze {
						return strings.Compare(left.Country, right.Country) == -1
					}

					return left.Bronze > right.Bronze
				}

				return left.Silver > right.Silver
			}

			return left.Gold > right.Gold
		})
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/athlete-info", func(w http.ResponseWriter, req *http.Request) {
		query := req.URL.Query()

		name := query.Get("name")

		athlete, found := preparedAthletes[name]

		if found {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(200)

			err = json.NewEncoder(w).Encode(athlete)

			if err != nil {
				log.Fatal(err)
			}
		} else {
			w.WriteHeader(404)
			fmt.Fprintf(w, "athlete not found")
		}
	})

	mux.HandleFunc("/top-athletes-in-sport", func(w http.ResponseWriter, req *http.Request) {
		query := req.URL.Query()

		sport := query.Get("sport")
		limit, err := strconv.Atoi(query.Get("limit"))

		if err != nil && query.Get("limit") != "" {
			w.WriteHeader(400)
			fmt.Fprintf(w, "wrong limit")

			return
		}

		sportInfo, found := sortedBySport[sport]

		if found {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(200)

			err = json.NewEncoder(w).Encode(sportInfo[:limit])

			if err != nil {
				log.Fatal(err)
			}
		} else {
			w.WriteHeader(404)
			fmt.Fprintf(w, "sport not found")
		}
	})

	mux.HandleFunc("/top-countries-in-year", func(w http.ResponseWriter, req *http.Request) {
		query := req.URL.Query()

		year, _ := strconv.Atoi(query.Get("year"))
		limit, err := strconv.Atoi(query.Get("limit"))

		if err != nil && query.Get("limit") != "" {
			w.WriteHeader(400)
			fmt.Fprintf(w, "wrong limit")

			return
		}

		yearInfo, found := countryData[year]

		if found {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(200)

			realLimit := limit

			if len(yearInfo) < limit {
				realLimit = len(yearInfo)
			}

			err = json.NewEncoder(w).Encode(yearInfo[:realLimit])

			if err != nil {
				log.Fatal(err)
			}
		} else {
			w.WriteHeader(404)
			fmt.Fprintf(w, "year not found")
		}
	})

	log.Fatal(http.ListenAndServe(":"+*portPtr, mux))
}
