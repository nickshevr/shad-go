// +build !solution

package otp

import (
	"io"
)

type StreamCipherReader struct {
	r    io.Reader
	prng io.Reader
}

type StreamCipherWriter struct {
	w    io.Writer
	prng io.Reader
}

func (x StreamCipherWriter) Write(p []byte) (n int, err error) {
	buffSize := len(p)
	data := make([]byte, buffSize)
	xorPart := make([]byte, buffSize)

	var processData = func(size int) {
		for i := 0; i < size; i++ {
			data[i] = xorPart[i] ^ p[i]
		}
	}

	_, _ = x.prng.Read(xorPart)

	processData(buffSize)

	return x.w.Write(data)
}

func (x StreamCipherReader) Read(p []byte) (n int, err error) {
	buffSize := len(p)

	for {
		data := make([]byte, buffSize)

		dataReaded, err := x.r.Read(data)

		xorPart := make([]byte, dataReaded)

		_, _ = x.prng.Read(xorPart)

		var processData = func(size int) {
			for i := 0; i < size; i++ {
				p[i] = xorPart[i] ^ data[i]
			}
		}

		processData(dataReaded)

		return dataReaded, err
	}
}

func NewReader(r io.Reader, prng io.Reader) io.Reader {
	return StreamCipherReader{r, prng}
}

func NewWriter(w io.Writer, prng io.Reader) io.Writer {
	return StreamCipherWriter{w, prng}
}
