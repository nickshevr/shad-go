// +build !solution

package retryupdate

import (
	"errors"
	"github.com/gofrs/uuid"
	"gitlab.com/slon/shad-go/retryupdate/kvapi"
)

func GenerateUUID() uuid.UUID {
	return uuid.Must(uuid.NewV4())
}

func UpdateValue(c kvapi.Client, key string, updateFn func(oldValue *string) (newValue string, err error)) error {
	var err error
	var gResp *kvapi.GetResponse
	var newValue string
	var authErr *kvapi.AuthError
	var conflictErr *kvapi.ConflictError
	var previousVersion uuid.UUID

	for {
		gResp, err = c.Get(&kvapi.GetRequest{
			Key: key,
		})

		switch {
		case errors.As(err, &authErr):
			return err
		case err != nil:
			if err.Error() == `api: "get" error: key not found` {
				break
			} else {
				continue
			}
		}

		if gResp == nil {
			newValue, err = updateFn(nil)
		} else {
			newValue, err = updateFn(&gResp.Value)
		}

		if err != nil {
			return err
		}

		break
	}

	for {
		rec := kvapi.SetRequest{
			Key:   key,
			Value: newValue,
		}

		if gResp != nil {
			rec.OldVersion = gResp.Version
			rec.NewVersion = GenerateUUID()
		}

		_, err = c.Set(&rec)

		switch {
		case errors.As(err, &authErr):
			return err
		case errors.As(err, &conflictErr):
			if conflictErr.ExpectedVersion == previousVersion {
				return nil
			}

			return UpdateValue(c, key, updateFn)
		case err != nil:
			if err.Error() == `api: "set" error: key not found` {
				newValue, _ = updateFn(nil)
				gResp = nil
			}

			previousVersion = rec.NewVersion
		case err == nil:
			return nil
		}
	}
}
