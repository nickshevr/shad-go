// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	urls := os.Args[1:]

	for _, url := range urls {
		resp, err := http.Get(url)

		check(err)

		body, err := ioutil.ReadAll(resp.Body)

		resp.Body.Close()

		check(err)

		fmt.Printf("%s", body)
	}
}
