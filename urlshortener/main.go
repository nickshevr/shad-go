// +build !solution

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand = rand.New(
	rand.NewSource(42))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func String(length int) string {
	return StringWithCharset(length, charset)
}

var (
	urlKEY map[string]string
	keyURL map[string]string
	mu     sync.Mutex
)

type ShortenBody struct {
	URL string `json:"url"`
}

type ShortenResponse struct {
	URL string `json:"url"`
	Key string `json:"key"`
}

func main() {
	portPtr := flag.String("port", ":6029", "setting a port for web server")
	urlKEY := make(map[string]string)
	keyURL := make(map[string]string)
	flag.Parse()

	mux := http.NewServeMux()
	mux.HandleFunc("/shorten", func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "POST" {
			var bodyData ShortenBody
			buffer, _ := ioutil.ReadAll(req.Body)

			err := json.Unmarshal(buffer, &bodyData)

			if err != nil {
				w.WriteHeader(400)
				fmt.Fprintf(w, "invalid request")

				return
			}

			mu.Lock()

			if urlKEY[bodyData.URL] == "" {
				key := String(10)
				urlKEY[bodyData.URL] = key
				keyURL[key] = bodyData.URL
			}

			mu.Unlock()

			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(200)

			err = json.NewEncoder(w).Encode(ShortenResponse{
				URL: bodyData.URL,
				Key: urlKEY[bodyData.URL],
			})

			if err != nil {
				w.WriteHeader(500)
			}
		}
	})

	mux.HandleFunc("/go/", func(w http.ResponseWriter, req *http.Request) {
		parts := strings.Split(req.URL.String(), "/")
		key := parts[len(parts)-1]
		url := keyURL[key]

		if url != "" {
			w.Header().Add("Location", url)
			w.WriteHeader(302)

			return
		}

		w.WriteHeader(404)
		fmt.Fprintf(w, "key not found")
	})

	log.Fatal(http.ListenAndServe(":"+*portPtr, mux))
}
