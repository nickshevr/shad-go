// +build !solution

package reverse

func Reverse(input string) string {
	var result string

	for _, rune := range input {
		result = string(rune) + result
	}

	return result
}
