// +build !solution

package spacecollapse

import "unicode"

func CollapseSpaces(input string) string {
	lastWasSpace := false
	var result string

	for _, rune := range input {
		if unicode.IsSpace(rune) {
			if lastWasSpace {
				continue
			}

			lastWasSpace = true

			result += " "
		} else {
			lastWasSpace = false

			result += string(rune)
		}
	}

	return result
}
