// +build !solution

package varfmt

import (
	"fmt"
	"strconv"
	"strings"
)

func Sprintf(format string, args ...interface{}) string {
	words := strings.Split(format, " ")
	builder := strings.Builder{}
	builder.Grow(len(format) * 8)
	currentArg := 0

	argsMap := make(map[int]string)

	for i, value := range args {
		if value != nil {
			argsMap[i] = fmt.Sprint(value)
		}
	}

	for i, word := range words {
		parts := strings.Split(word, "{")

		for _, part := range parts {
			if part == "" {
				continue
			}

			if part[len(part)-1] == '}' {
				// { }
				if len(part) == 1 {
					arg := argsMap[currentArg]
					builder.WriteString(arg)
				} else {
					argIndex, _ := strconv.Atoi(part[:len(part)-1])
					arg := argsMap[argIndex]
					builder.WriteString(arg)
				}

				currentArg++
			} else {
				builder.WriteString(part)
				currentArg++
			}
		}

		if i+1 != len(words) {
			builder.WriteString(" ")
		}
	}

	return builder.String()
}
