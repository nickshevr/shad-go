// +build !solution

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	s "strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readFile(path string) string {
	dat, err := ioutil.ReadFile(path)
	check(err)

	return string(dat)
}

func parseStroke(data string) []string {
	return s.Split(data, "\n")
}

func main() {
	fileNames := os.Args[1:]
	m := make(map[string]int)

	for i := 0; i < len(fileNames); i++ {
		fileName := fileNames[i]

		data := readFile(fileName)
		strokes := parseStroke(data)

		for j := 0; j < len(strokes); j++ {
			stroke := strokes[j]

			_, prs := m[stroke]

			if !prs {
				m[stroke] = 1
			} else {
				m[stroke]++
			}
		}
	}

	for k, v := range m {
		if v > 1 {
			fmt.Printf("%d\t%s\n", v, k)
		}
	}
}
